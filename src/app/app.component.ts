import { Component } from "@angular/core";
import { SwPush } from "../../node_modules/@angular/service-worker";
import { HttpClient } from "@angular/common/http";


@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  readonly VAPID_PUBLIC_KEY =
    'BDsh-HospmtEmRiz70mO-SBsL_5WAIoYJ59sJ9OSqOawXXwBUPw3wIOENbsowuuuWiAvm-T0-MO_fR1tq4CxYd0';
  title = "test-55";

  constructor(private swPush: SwPush, private httpClient: HttpClient) { }
  businessCardClick({ action, user }) {
    console.log(user);
    let userId = user.userId;
    console.log(userId);
    this.httpClient.post(`http://localhost:50943/notifications/sendNotification/${userId}`, null).subscribe();
  }

  subscribeToNotifications() {
    this.swPush.requestSubscription({
      serverPublicKey: this.VAPID_PUBLIC_KEY
    }).then(sub => {
      // var authKey = String.fromCharCode.apply(null, new Uint16Array(sub.getKey('auth')));
      // var p256dhKey = String.fromCharCode.apply(null, new Uint16Array(sub.getKey('p256dh')));
      var jsonSubscription = JSON.stringify(sub);
      var jsonObject = JSON.parse(jsonSubscription);
      console.log('test');
      console.log(jsonObject);

      let subscription = {
        Auth: jsonObject.keys['auth'],
        P256DH: jsonObject.keys['p256dh'],
        Endpoint: sub.endpoint,
        Id: 8601293
      };

      this.httpClient.post('http://localhost:50943/notifications/createSubscription', subscription).subscribe();

    });
  }

}
